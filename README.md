# House Prices - Advanced Regression Techniques

This is a Notebook for the Kaggle competition, House Prices - Advanced Regression Techniques: 

https://www.kaggle.com/competitions/house-prices-advanced-regression-techniques/overview

## Goal

The goal of notebook is to predict the final price of each home using 79 explanatory variables describing (almost) every aspect of residential homes in Ames, Iowa.

## Evaluation

Submissions are evaluated on Root-Mean-Squared-Error (RMSE) between the logarithm of the predicted value and the logarithm of the observed sales price.

## Main Contents 

- Linear Regression
- Ridge Regression
- XGB Regressor

## Kaggle's notebooks:

https://www.kaggle.com/code/mr0024/house-prices-using-ridge-regression

https://www.kaggle.com/code/mr0024/house-prices-using-ridge-and-xgb

## Requirements

The requirements file contains all the packages needed to work through this notebooks
